import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
import 'rxjs/Rx';


@Injectable()
export class MessagesService {
  http:Http; //http is the name, Http is the type
  getMessages(){
   //return ['Message1', 'Message2', 'Message3', 'message4'];
   //get messages from the SLIM rest API (dont say DB)
   let token = localStorage.getItem('token');
   //מוסיפים טוקן להדר של הבקשה
   let options = {
     headers:new Headers({
       'Authorization':'Bearer '+token
     })
   }
   return this.http.get(environment.url + 'messages',options); 

  }

  getMessagesFire(){
    return this.db.list('/messages').valueChanges();
  
  }

  getMessage(id){
    let token = localStorage.getItem('token');
   //מוסיפים טוקן להדר של הבקשה
   let options = {
     headers:new Headers({
       'Authorization':'Bearer '+token
     })
   }
   return this.http.get(environment.url + 'messages/'+id,options);
  }

  postMessage(data){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message', data.message);
    return this.http.post(environment.url + 'messages', params.toString(), options);
    
  }

  deleteMessage(key){
    return this.http.delete(environment.url + 'messages/' +key);
  }

  login(credentials){
     let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
     }
    //append  משמעותו להוסיף את השדות
      let params = new HttpParams().append('user', credentials.user).append('password', credentials.password);
      return this.http.post(environment.url + 'auth', params.toString(), options).map(response=>{
        let token= response.json().token;
        if (token) localStorage.setItem('token',token);
        console.log(token);
      });

  } 

  constructor(http:Http, private db:AngularFireDatabase) { //http is a new object of type Http
    this.http = http;
  }

}