// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase:{
     apiKey: "AIzaSyAhkVkw5UbsF0lO7lT5n-nSw_CAzATHN0Y",
    authDomain: "messages-5452d.firebaseapp.com",
    databaseURL: "https://messages-5452d.firebaseio.com",
    projectId: "messages-5452d",
    storageBucket: "messages-5452d.appspot.com",
    messagingSenderId: "834628364557"
  }
};
